const database = require('./db/database');
const express = require('express');
const contestCreatorCronJob = require('./cronJobs/contestCreatorCronJob');
const contestRoute = require('./routes/contestRoute');
const playerRoute = require('./routes/playerRoute');
const teamRoute = require('./routes/teamRoute');
const cors = require('cors');
const app = express();

function startServer() {
    database.connect().then(() => {
        app.use(express.json());
        app.use(cors(
            {origin: 'http://localhost:3000'}
        ));

        useRoutes();
        startCronJobs();

        const port = 3000;
        app.listen(port, () => console.log(`Listening in port ${port}...`));
    });
}

function useRoutes() {
    app.use('/api/contests', contestRoute);
    app.use('/api/players', playerRoute);
    app.use('/api/teams', teamRoute);
}

function startCronJobs() {
    contestCreatorCronJob.startJob();
}

startServer();

module.exports = app;