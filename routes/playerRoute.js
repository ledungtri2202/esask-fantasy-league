const router = require('express').Router();
const playerController = require('../controllers/playerController');

router.get('/', playerController.getAllPlayers);

router.get('/:id', playerController.getPlayerById);

module.exports = router;