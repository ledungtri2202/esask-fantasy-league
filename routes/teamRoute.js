const router = require('express').Router();
const teamController = require('../controllers/teamController');

router.post('/', teamController.createTeam);

router.get('/:id', teamController.getTeamById);

router.put('/:id', teamController.updateTeamById);

module.exports = router;