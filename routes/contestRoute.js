const router = require('express').Router();
const contestController = require('../controllers/contestController');

router.get('/', contestController.getAllContests);

router.get('/:id', contestController.getContestById);

module.exports = router;