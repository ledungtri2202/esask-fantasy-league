const {CronJob} = require('cron');

async function startJob() {
    const job = new CronJob("0 0 * * SAT", createWeeklyContest, null, true);
    job.start();
}

async function createWeeklyContest() {
    const startDate = new Date();
    startDate.setDate(startDate.getDate() + 7);
    startDate.setHours(0,0,0,0);

    const endDate = new Date();
    endDate.setDate(endDate.getDate() + 14);
    endDate.setHours(0,0,0,0);

    const contest = {
        name: `Weekly Contest ${startDate.toLocaleDateString()} - ${endDate.toLocaleDateString()}`,
        startDate: startDate,
        endDate: endDate
    };

    await contest.create(contest);
}

module.exports = {startJob};