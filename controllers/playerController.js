const playerService = require('../services/playerService');

async function getAllPlayers(req, res) {
    const result = await playerService.getAllPlayers();
    return res.status(200).json({players: result});
}

async function getPlayerById(req, res) {
    try {
        const player = await playerService.getPlayerById(req.params.id);
        return res.status(200).json({player: player});
    } catch (error) {
        return res.status(400).json({error: error.message});
    }
}

module.exports = {getAllPlayers, getPlayerById};