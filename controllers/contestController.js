const contestService = require('../services/contestService');
const ObjectId = require('mongoose').Types.ObjectId;

async function getAllContests(req, res) {
    const contests = contestService.getAllContests();
    return res.status(200).json({contests: contests});
}

async function getContestById(req, res) {
    if (!ObjectId.isValid(req.params.id)) {
        return res.status(400).send("Invalid id");
    }

    try {
        const contest = await contestService.getContestById(req.params.id);
        return res.status(200).json({contest: contest});
    } catch (error) {
        return res.status(400).json({error: error.message});
    }
}

module.exports = {getAllContests, getContestById};