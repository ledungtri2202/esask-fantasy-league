const Contest = require('../models/contest');
const Team = require('../models/team');
const playerService = require('./playerService');
const ObjectId = require('mongoose').Types.ObjectId;

async function createTeam(body) {
    const team = new Team({
        name: body.name.trim(),
        contestId: body.contestId,
        players: body.players || []
    });
    await validateContest(body.contestId);
    await team.save();

    return getTeamById(team.id);
}


async function getTeamById(id) {
    const team = await getTeam(id);
    team.players.map(player => playerService.getPlayerById(player.id));
    return team;
}

async function updateTeamById(id, body) {
    const team = await getTeam(id);
    team.name = body.name;
    team.players = body.players;
    await team.save();

    return getTeamById(id);
}

async function getTeam(id) {
    if (!ObjectId.isValid(id)) {
        throw {error: {message: 'Invalid id'}};
    }

    const team = await Team.findById(id);
    if (!team) {
        throw {error: {message: 'Team does not exist'}};
    }

    return team;
}

async function validateContest(contestId) {
    if (!ObjectId.isValid(contestId)) {
        throw {message: "Invalid contestId"};
    }

    const contest = await Contest.findById(contestId);
    if (!contest) {
        throw {message: "Contest does not exist"};
    }

    // todo: check contest closed
}

module.exports = {createTeam, getTeamById, updateTeamById};