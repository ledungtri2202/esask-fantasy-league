const riotRequest = require('../services/riotRequestService');

async function getAllPlayers() {
    const players = await riotRequest.getPlayers();
    const playersWithValue = assignPlayerValues(players);
    return playersWithValue;
}

async function getPlayerById(id) {
    const player = await riotRequest.getPlayerById(id);
    const performance = await getPerformance(player.puuid);
    const score = calculateScore(performance);
    return {name: player.name, id: player.id, performance, score};
}

async function getPerformance(puuid) {
    // TODO: take in time frame
    const matchesIds = await riotRequest.getPlayerMatchIds(puuid);
    const performance = {
        kills: 0,
        assists: 0,
        deaths: 0,
        teamBonuses: { towers: 0, dragons: 0, barons: 0, wins: 0 }
    };

    await Promise.all(matchesIds.map(async matchId => {
        const match = await riotRequest.getMatchById(matchId);
        const participants = match.info.participants;

        // Calculate player's performance
        const participant = participants.find(p => p.puuid === puuid);
        performance.kills += participant.kills;
        performance.assists += participant.assists;
        performance.deaths += participant.deaths;

        // Calculate team bonuses
        const teamId = participant.teamId;
        const team = match.info.teams.find(t => t.teamId === teamId);
        performance.teamBonuses.towers += team.objectives.tower.kills;
        performance.teamBonuses.dragons += team.objectives.dragon.kills;
        performance.teamBonuses.barons += team.objectives.baron.kills;
        if (team.win) {
            performance.teamBonuses.wins += 1;
        }
    }));
    return performance;
}

function assignPlayerValues(players) {
    const sorted = players.sort((a, b) => (a.leaguePoints < b.leaguePoints) ? 1 : -1);
    const result = sorted.map(player => {
        const playerObj = {
            id: player.summonerId,
            name: player.summonerName,
            value: 0
        };

        if (sorted.indexOf(player) < 40) {
            playerObj.value = 12500;
        } else if (sorted.indexOf(player) < 100) {
            playerObj.value = 10000;
        } else if (sorted.indexOf(player) < 200) {
            playerObj.value = 7500;
        }
        return playerObj;
    });
    return result;
}

function calculateScore(performance, isCaptain = false) {
    let score = performance.kills * 3 + performance.assists * 1.5 - performance.deaths;
    if (isCaptain) {
        score *= 1.5;
        const teamBonuses = performance.teamBonuses;
        score += teamBonuses.towers + teamBonuses.dragons * 2 + teamBonuses.barons * 3 + teamBonuses.wins * 2;
    }
    return score;
}

module.exports = {getAllPlayers, getPlayerById};