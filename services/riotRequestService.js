const GaleforceModule = require('galeforce');
const galeforce = new GaleforceModule({'riot-api': {key: 'RGAPI-e91f7b3c-8472-4ee2-a1a5-2680c0147d9b'}});

async function getPlayers() {
    const response = await galeforce.lol.league.league()
        .region(galeforce.region.lol.NORTH_AMERICA)
        .queue(galeforce.queue.lol.RANKED_SOLO)
        .tier(galeforce.tier.GRANDMASTER)
        .exec();
    return response.entries;
}

async function getPlayerById(id) {
    return galeforce.lol.summoner()
        .region(galeforce.region.lol.NORTH_AMERICA)
        .summonerId(id)
        .exec();
}

function getPlayerMatchIds(puuid, startDate = null, endDate = null) {
    const query = {};
    if (startDate) {
        query.startTime = Math.floor(startDate.getTime() / 1000);
    }
    if (endDate) {
        query['endTime'] = Math.floor(endDate.getTime() / 1000);
    }
    return galeforce.lol.match.list()
        .region(galeforce.region.riot.AMERICAS)
        .puuid(puuid)
        .query(query)
        .exec();
}

function getMatchById(matchId) {
    return galeforce.lol.match.match().region(galeforce.region.riot.AMERICAS).matchId(matchId).exec();
}

module.exports = {getPlayers, getPlayerById, getPlayerMatchIds, getMatchById};