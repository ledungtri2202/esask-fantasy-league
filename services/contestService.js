const Contest = require('../models/contest');

async function getAllContests() {
    return Contest.find();
    // TODO: filter closed contest
}

async function getContestById(id) {
    return Contest.findById(id);
    // TODO: get participated teams

}

module.exports = {getAllContests, getContestById};
